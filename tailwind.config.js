/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    lineHeight: {
      btn: 3
    }
  },
  variants: { borderRadius: ['responsive', 'hover', 'focus'] },
  plugins: []
}
