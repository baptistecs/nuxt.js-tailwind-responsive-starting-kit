module.exports = {
  semi: false,
  arrowParens: 'always',
  singleQuote: true,
  // "useTabs": true,
  tabWidth: 2,
  printWidth: 80
}
